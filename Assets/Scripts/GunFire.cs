﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class GunFire : MonoBehaviour {
	GameObject ch;
	// Use this for initialization
	void Start () {

		ch = GameObject.FindGameObjectWithTag ("Crosshair");
	}
	
	// Update is called once per frame
	void Update () {
		Animation gunAnim = GetComponent<Animation> ();
		List<string> gunAnimNames = new List<string> ();
		foreach (AnimationState state in gunAnim) {
			gunAnimNames.Add (state.name);
		}
		if (Input.GetButtonDown ("Fire1") && GlobalAmmo.currentAmmo > 0) {
			AudioSource gunSound = GetComponent<AudioSource> ();
			gunSound.Play ();
			gunAnim.Play ("GunShot");
			GlobalAmmo.currentAmmo--;
		}
		if(Input.GetButtonDown("Fire2")){
			gunAnim.Play ("ADS");
			ch.SetActive (false);
		}
		if(Input.GetButtonUp("Fire2")){
			gunAnim.Play ("HipFire");
			ch.SetActive (true);
		}
	}
}
